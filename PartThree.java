import java.util.Scanner;

public class PartThree
{
  public static void main(String[] args)
  {
    Scanner scan = new Scanner(System.in);
    Calculator calc = new Calculator();
    
    System.out.println("Enter a number.");
    double firstNum = scan.nextDouble();
    
    System.out.println("Enter another number.");
    double secondNum = scan.nextDouble();
    
    System.out.println("Sum: " + Calculator.add(firstNum, secondNum));
    System.out.println("Difference: " + Calculator.subtract(firstNum, secondNum));
    System.out.println("Product: " + calc.multiply(firstNum, secondNum));
    System.out.println("Quotient: " + calc.divide(firstNum, secondNum));
  }
}