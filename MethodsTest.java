public class MethodsTest
{
	public static void main(String[] args)
	{
		int x = 5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
    
		double y = 6.5;
		methodTwoInputNoReturn(x, y);
    
    int squarenum1 = 9;
    int squarenum2 = 5;
		double z = sumSquareRoot(squarenum1, squarenum2);
		System.out.println(z);
    
    String s1 = "java";
    String s2 = "programming";
    System.out.println(s1.length());
    System.out.println(s2.length());
    
    SecondClass sc = new SecondClass();
    System.out.println(SecondClass.addOne(50));
    System.out.println(sc.addTwo(50) );
	} 
  
	public static void methodNoInputNoReturn()
	{
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x = 20;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int x)
	{
		System.out.println("Inside the method one input no return");
		x = x-5;
		System.out.println(x);
	}
	
	public static void methodTwoInputNoReturn(int x, double y)
	{
		System.out.println("x: " + x + ", y: " + y);
	}
	
	public static int methodNoInputReturnInt()
	{
		int z  = 5;
		return z;
	}
	
	public static double sumSquareRoot(int x, int y)
	{
		int z = x + y;
		double answer = Math.sqrt(z);
		return answer;
	}
}