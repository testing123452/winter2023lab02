public class Calculator
{
  public static double add(double x, double y)
  {
    double sum = x + y;
    return sum;
  }
  
    public static double subtract(double x, double y)
  {
    double difference = x - y;
    return difference;
  }
  
    public double multiply(double x, double y)
  {
    double product = x * y;
    return product;  
  }
  
    public double divide(double x, double y)
  {
    double quotient = x / y;
    return quotient;
  }
}